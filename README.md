# README #

### What is this repository for? ###

* Plotting and exploring PCs
* Identify outliers and download included and/or excluded samples


### How do I get set up? ###

* Download the script (and example.data.txt)
* make sure you have the following R libraries: "plotly", "shiny", "shinyjs", "shinydashboard" and "optparse"
* Go to terminal
* run Rscript PCplots.R
* To also create .png images please include:
* --wd "/Users/Rick/Desktop/PCplot/" #working directory to write the png to
* --username ""                      #plotly username, sign up for free here: https://plot.ly/accounts/login/?action=login
* --apikey ""                        #after you have your plotly account, go to Settings --> API Keys and generate+copy your API key

### Who do I talk to? ###

* Rick van der Spek